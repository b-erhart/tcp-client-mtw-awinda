//
// Created by Nicholas on 29.03.2021.
//

#ifndef FORMATTER_H
#define FORMATTER_H

#include <fstream>
#include <vector>
#include <nlohmann/json.hpp>
#include <xsensdeviceapi.h>
#include <xstypes/xstime.h>


class DataFormatter {

    std::fstream fileOutputStream;

public:

    DataFormatter();

    void open(const std::string& filename, int deviceCount);

    void addVector(std::vector<XsCalibratedData> data);

    void close();

    std::string getJson(std::vector<XsCalibratedData> data);

};


#endif // #ifndef FORMATTER_H
