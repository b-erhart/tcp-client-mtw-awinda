//
// Created by Nicholas on 29.03.2021.
//

#include "data_formatter.h"
#include <xsensdeviceapi.h>
#include <ios>
#include <iostream>
#include <string>
#include <locale>

DataFormatter::DataFormatter() = default;

void DataFormatter::open(const std::string& filename, int deviceCount) {
    std::string x;
    std::string y;
    std::string z;

    fileOutputStream.open(filename, std::ios::out | std::ios::app);

    for (int i{0}; i < deviceCount; i++) {
        x = "X" + std::to_string(i);
        y = "Y" + std::to_string(i);
        z = "Z" + std::to_string(i);
        if (i == deviceCount - 1)
            fileOutputStream << x << ";" << y << ";" << z;
        else
            fileOutputStream << x << ";" << y << ";" << z << ";";
    }

    fileOutputStream << "\n";
}

void DataFormatter::addVector(std::vector<XsCalibratedData> data) {
    for (int i{0}; i < data.size(); i++) {
        if (i == data.size() - 1)
            fileOutputStream << data[i].m_acc[0] << ";" << data[i].m_acc[1] << ";" << data[i].m_acc[2] << ";" << "\n";
        else
            fileOutputStream << data[i].m_acc[0] << ";" << data[i].m_acc[1] << ";" << data[i].m_acc[2] << ";";
    }
}

void DataFormatter::close() {
    fileOutputStream.close();
}

std::string DataFormatter::getJson(std::vector<XsCalibratedData> data) {
    nlohmann::json json;
    for (int i{0}; i < data.size(); i++) {
        json[i] = {{"X", data[i].m_acc[0]},
                   {"Y", data[i].m_acc[1]},
                   {"Z", data[i].m_acc[2]}};
    }

    return json.dump();
}
