//
// Created by Benedikt on 16.04.2021.
//

#ifndef AWINDASERVERCLION_CONFIG_H
#define AWINDASERVERCLION_CONFIG_H

const int desiredUpdateRate = 75;      // update rate for MTWs in Hz
const int desiredRadioChannel = 19;    // radio channel for wireless master.
const std::string tcpServerAddress = "localhost";
const std::string tcpServerPort = "2000";

#endif // #ifndef AWINDASERVERCLION_CONFIG_H
