# TCP Client for Xsens MTw Awinda

This is a small TCP Client written in C++ for the Xsens MTw Awinda sensors utilizing the Xsens MT SDK. It connects to
the dongle/hub and waits for the sensors to connect. Then it goes on to connect to a TCP server with the address and
port specified in the corresponding variables. Once a connection is established, it will start to stream the recorded
data from the sensors to the server.

This makes it easy to process the sensor data with different tools like MATLAB or programs in various programming
languages. You only have to receive the data via TCP and then process it accordingly.


## Build instructions
> **Note:** The building process was only tested on 64 bit Windows 10 machines. You may be able to use it on other
> systems after applying some minor tweaks though.

It is recommended to build this project using the [JetBrains CLion IDE](https://www.jetbrains.com/clion/) alongside
[MinGW](http://mingw-w64.org) (make sure to install the 64 bit version).


### Prerequisites
You should have the [vcpkg](https://docs.microsoft.com/de-de/cpp/build/vcpkg?view=msvc-160) package manager installed to
easily download the required dependencies for this project. Once you have this set up, get the following packages:

- [nlohmann-json](https://github.com/nlohmann/json) - `vcpkg install nlohmann-json:x64-windows`

Once you ran all the vcpkg commands listed above, use `vcpkg integrate install` to complete the setup. Then make sure
that your CLion installation will pick up the installed packages. To do so, go to
`File > Settings > Build, Execution, Deployment > CMake` and add the following line to the `CMake options` text field:

```
-DCMAKE_TOOLCHAIN_FILE="C:/Program Files/vcpkg/scripts/buildsystems/vcpkg.cmake" -DVCPKG_APPLOCAL_DEPS=OFF
```

Make sure to adjust the path to your vcpkg installation accordingly.

> **Tip**: Add the root folder of your vcpkg installation (which contains `vcpkg.exe`) to your `Path` system environment
> variable. You can then use the `vcpkg` command from any directory.

### Building

First, clone this repository using the command:

```git
git clone https://gitlab.com/b-erhart/tcp-client-mtw-awinda.git
```

Then, open the newly cloned folder in CLion and click the little hammer symbol in the top right corner to build the
project. This will create an .exe file which you can launch to stream data from your Awinda sensors to a TCP server of
your choice.

## Attributions
- The x64 libs and includes from the [Xsens MT SDK](https://www.xsens.com/software-downloads) are used to communicate
  with the sensors
- The TCPClient and Socket implementations from [socket-cpp](https://github.com/embeddedmz/socket-cpp) are used to operate
the TCP client
