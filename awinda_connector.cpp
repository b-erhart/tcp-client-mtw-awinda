//
// Created by Benedikt on 16.04.2021.
//

//  Copyright (c) 2003-2019 Xsens Technologies B.V. or subsidiaries worldwide.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification,
//  are permitted provided that the following conditions are met:
//
//  1.	Redistributions of source code must retain the above copyright notice,
//  	this list of conditions, and the following disclaimer.
//
//  2.	Redistributions in binary form must reproduce the above copyright notice,
//  	this list of conditions, and the following disclaimer in the documentation
//  	and/or other materials provided with the distribution.
//
//  3.	Neither the names of the copyright holders nor the names of their contributors
//  	may be used to endorse or promote products derived from this software without
//  	specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
//  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
//  THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
//  OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
//  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY OR
//  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.THE LAWS OF THE NETHERLANDS
//  SHALL BE EXCLUSIVELY APPLICABLE AND ANY DISPUTES SHALL BE FINALLY SETTLED UNDER THE RULES
//  OF ARBITRATION OF THE INTERNATIONAL CHAMBER OF COMMERCE IN THE HAGUE BY ONE OR MORE
//  ARBITRATORS APPOINTED IN ACCORDANCE WITH SAID RULES.
//

#include <iostream>
#include <conio.h>

#include "socket_cpp/TCPClient.h"

#include <xsensdeviceapi.h>

#include "config.h"
#include "stream_overloads.h"
#include "callbacks/wireless_master_callback.h"
#include "callbacks/mtw_callback.h"
#include "utils/data_formatter.h"

/*! \brief Given a list of update rates and a desired update rate, returns the closest update rate to the desired one */
int findClosestUpdateRate(const XsIntArray &supportedUpdateRates, const int desiredUpdateRate) {
    if (supportedUpdateRates.empty()) {
        return 0;
    }

    if (supportedUpdateRates.size() == 1) {
        return supportedUpdateRates[0];
    }

    int uRateDist = -1;
    int closestUpdateRate = -1;
    for (XsIntArray::const_iterator itUpRate = supportedUpdateRates.begin();
         itUpRate != supportedUpdateRates.end(); ++itUpRate) {
        const int currDist = std::abs(*itUpRate - desiredUpdateRate);

        if ((uRateDist == -1) || (currDist < uRateDist)) {
            uRateDist = currDist;
            closestUpdateRate = *itUpRate;
        }
    }
    return closestUpdateRate;
}

XsControl* constructXsControl() {
    std::cout << "Constructing XsControl..." << std::endl;
    XsControl *control = XsControl::construct();
    if (control == nullptr) {
        std::cout << "Failed to construct XsControl instance." << std::endl;
    }

    return control;
}

void disconnectWirelessMaster(XsDevicePtr &wirelessMasterDevice) {
    std::cout << "Setting config mode..." << std::endl;
    if (!wirelessMasterDevice->gotoConfig()) {
        std::ostringstream error;
        error << "Failed to goto config mode: " << *wirelessMasterDevice;
        throw std::runtime_error(error.str());
    }

    std::cout << "Disabling radio... " << std::endl;
    if (!wirelessMasterDevice->disableRadio()) {
        std::ostringstream error;
        error << "Failed to disable radio: " << *wirelessMasterDevice;
        throw std::runtime_error(error.str());
    }
}

int main(int argc, char *argv[]) {
    WirelessMasterCallback wirelessMasterCallback; // Callback for wireless master
    std::vector<MtwCallback *> mtwCallbacks; // Callbacks for callbacks devices

    auto LogPrinter = [](const std::string &strLogMsg) { std::cout << strLogMsg << std::endl; };

    XsControl *control = constructXsControl();

    try {
        // Detect port for wireless master
        std::cout << "Scanning ports..." << std::endl;
        XsPortInfoArray detectedDevices = XsScanner::scanPorts();

        std::cout << "Finding wireless master..." << std::endl;
        XsPortInfoArray::const_iterator wirelessMasterPort = detectedDevices.begin();
        while (wirelessMasterPort != detectedDevices.end() && !wirelessMasterPort->deviceId().isWirelessMaster()) {
            ++wirelessMasterPort;
        }
        if (wirelessMasterPort == detectedDevices.end()) {
            throw std::runtime_error("No wireless masters found");
        }
        std::cout << "Wireless master found @ " << *wirelessMasterPort << std::endl;

        std::cout << "Opening port..." << std::endl;
        if (!control->openPort(wirelessMasterPort->portName().toStdString(), wirelessMasterPort->baudrate())) {
            std::ostringstream error;
            error << "Failed to open port " << *wirelessMasterPort;
            throw std::runtime_error(error.str());
        }



        // Get wireless master instance
        std::cout << "Getting XsDevice instance for wireless master..." << std::endl;
        XsDevicePtr wirelessMasterDevice = control->device(wirelessMasterPort->deviceId());
        if (wirelessMasterDevice == nullptr) {
            std::ostringstream error;
            error << "Failed to construct XsDevice instance: " << *wirelessMasterPort;
            throw std::runtime_error(error.str());
        }

        std::cout << "XsDevice instance created @ " << *wirelessMasterDevice << std::endl;



        // Configure wireless master
        std::cout << "Setting config mode..." << std::endl;
        if (!wirelessMasterDevice->gotoConfig()) {
            std::ostringstream error;
            error << "Failed to goto config mode: " << *wirelessMasterDevice;
            throw std::runtime_error(error.str());
        }

        std::cout << "Attaching callback handler..." << std::endl;
        wirelessMasterDevice->addCallbackHandler(&wirelessMasterCallback);

        std::cout << "Getting the list of the supported update rates..." << std::endl;
        const XsIntArray supportedUpdateRates = wirelessMasterDevice->supportedUpdateRates();

        std::cout << "Supported update rates: ";
        for (XsIntArray::const_iterator itUpRate = supportedUpdateRates.begin();
             itUpRate != supportedUpdateRates.end(); ++itUpRate) {
            std::cout << *itUpRate << " ";
        }
        std::cout << std::endl;

        const int newUpdateRate = findClosestUpdateRate(supportedUpdateRates, desiredUpdateRate);

        std::cout << "Setting update rate to " << newUpdateRate << " Hz..." << std::endl;
        if (!wirelessMasterDevice->setUpdateRate(newUpdateRate)) {
            std::ostringstream error;
            error << "Failed to set update rate: " << *wirelessMasterDevice;
            throw std::runtime_error(error.str());
        }



        // Configure radio channel
        std::cout << "Disabling radio channel if previously enabled..." << std::endl;
        if (wirelessMasterDevice->isRadioEnabled()) {
            if (!wirelessMasterDevice->disableRadio()) {
                std::ostringstream error;
                error << "Failed to disable radio channel: " << *wirelessMasterDevice;
                throw std::runtime_error(error.str());
            }
        }

        std::cout << "Setting radio channel to " << desiredRadioChannel << " and enabling radio..." << std::endl;
        if (!wirelessMasterDevice->enableRadio(desiredRadioChannel)) {
            std::ostringstream error;
            error << "Failed to set radio channel: " << *wirelessMasterDevice;
            throw std::runtime_error(error.str());
        }



        // Connect MTW devices
        std::cout << "Waiting for MTW to wirelessly connect..." << std::endl;
        std::cout << "Press [Esc] to exit the program.\n" << std::endl;

        bool waitForConnections = true;
        size_t connectedMTWCount = wirelessMasterCallback.getWirelessMTWs().size();
        do {
            XsTime::msleep(100);

            while (true) {
                size_t nextCount = wirelessMasterCallback.getWirelessMTWs().size();
                if (nextCount != connectedMTWCount) {
                    std::cout << "Number of connected MTWs: " << nextCount << ". Press 'Y' to start measurement.\n"
                              << std::endl;
                    connectedMTWCount = nextCount;
                } else {
                    break;
                }
            }
            if (_kbhit()) {
                const char key = toupper((char) _getch());

                if (key == 27) {
                    std::cout << "Escape pressed. Exiting the program!\n" << std::endl;

                    disconnectWirelessMaster(wirelessMasterDevice);

                    throw std::runtime_error("");
                }

                waitForConnections = (key != 'Y');
            }
        } while (waitForConnections);



        // Data processing mode selection
        std::string processingMode = "0";
        DataFormatter theFormatter;
        CTCPClient tcpClient{ LogPrinter };

        do {
            std::cout << "Please select a data processing mode" << std::endl;
            std::cout << "1: Live transfer via TCP" << std::endl;
            std::cout << "2: Save as CSV file" << std::endl;
            std::cout << "9: Exit the program" << std::endl;
            std::cin >> processingMode;
            std::cout << std::endl;

            if (processingMode == "1") {
                bool waitForServerConnection = true;

                std::cout << "Connecting to server (" << tcpServerAddress << ":" << tcpServerPort << ")..." << std::endl;
                std::cout << "Press [Esc] to exit the program." << std::endl;

                bool connectedToServer{tcpClient.Connect(tcpServerAddress, tcpServerPort)};
                int timeSlept = 0;

                while (!connectedToServer) {
                    if (_kbhit()) {
                        const char key = toupper((char) _getch());

                        if (key == 27) {
                            std::cout << "Escape pressed. Exiting the program!\n" << std::endl;

                            disconnectWirelessMaster(wirelessMasterDevice);

                            throw std::runtime_error("");
                        }
                    }

                    if (timeSlept == 3000) {
                        connectedToServer = tcpClient.Connect("localhost", "2000");
                        timeSlept = 0;
                    }

                    XsTime::msleep(100);
                    timeSlept += 100;
                }

                std::cout << "Connected successfully" << std::endl;
            }
            else if (processingMode == "2") {
                std::string csvFilePath{};
                std::cout << "Enter the full path to the CSV file: ";
                std::cin >> csvFilePath;
                theFormatter.open(csvFilePath, mtwCallbacks.size());
            }
            else if (processingMode == "9") {
                std::cout << "Exiting the program" << std::endl;

                disconnectWirelessMaster(wirelessMasterDevice);

                throw std::runtime_error("");
            }
            else {
                std::cout << R"(Invalid selection. Please enter "1", "2" or "9" and hit enter.)" << std::endl;
                processingMode = "0";
            }

            std::cout << std::endl;
        } while (processingMode == "0");



        // Measurement
        std::cout << "Starting measurement..." << std::endl;
        if (!wirelessMasterDevice->gotoMeasurement()) {
            std::ostringstream error;
            error << "Failed to goto measurement mode: " << *wirelessMasterDevice;
            throw std::runtime_error(error.str());
        }

        std::cout << "Getting XsDevice instances for all MTWs..." << std::endl;
        XsDeviceIdArray allDeviceIds = control->deviceIds();
        XsDeviceIdArray mtwDeviceIds;
        for (XsDeviceIdArray::const_iterator i = allDeviceIds.begin(); i != allDeviceIds.end(); ++i) {
            if (i->isMtw()) {
                mtwDeviceIds.push_back(*i);
            }
        }
        XsDevicePtrArray mtwDevices;
        for (XsDeviceIdArray::const_iterator i = mtwDeviceIds.begin(); i != mtwDeviceIds.end(); ++i) {
            XsDevicePtr mtwDevice = control->device(*i);
            if (mtwDevice != nullptr) {
                mtwDevices.push_back(mtwDevice);
            } else {
                throw std::runtime_error("Failed to create an MTW XsDevice instance");
            }
        }

        std::cout << "Attaching callback handlers to MTWs..." << std::endl;
        mtwCallbacks.resize(mtwDevices.size());
        for (int i = 0; i < (int) mtwDevices.size(); ++i) {
            mtwCallbacks[i] = new MtwCallback(i, mtwDevices[i]);
            mtwDevices[i]->addCallbackHandler(mtwCallbacks[i]);
        }

        std::cout << "\nMain loop. Press any key to quit\n" << std::endl;
        std::cout << "Waiting for data available..." << std::endl;

        std::vector<XsEuler> eulerData(mtwCallbacks.size()); // Room to store euler data for each callbacks
        std::vector<XsCalibratedData> accData(mtwCallbacks.size());

        while (!_kbhit()) {
            XsTime::msleep(0);

            bool newDataAvailable = false;
            for (size_t i = 0; i < mtwCallbacks.size(); ++i) {
                if (mtwCallbacks[i]->dataAvailable()) {
                    newDataAvailable = true;
                    XsDataPacket const *packet = mtwCallbacks[i]->getOldestPacket();
                    eulerData[i] = packet->orientationEuler();
                    accData[i] = packet->calibratedData();
                    mtwCallbacks[i]->deleteOldestPacket();
                }
            }

            if (newDataAvailable) {
                if (processingMode == "1") {
                    bool sentSuccessfully = tcpClient.Send(theFormatter.getJson(accData) + "\n");

                    if (!sentSuccessfully) {
                        std::cout << "Exiting the program" << std::endl;

                        disconnectWirelessMaster(wirelessMasterDevice);

                        throw std::runtime_error("");
                    }
                }
                else if (processingMode == "2")
                    theFormatter.addVector(accData);
            }

        }
        (void) _getch();

        std::cout << std::endl;

        if (processingMode == "2" )
            theFormatter.close();

        disconnectWirelessMaster(wirelessMasterDevice);
    }
    catch (std::exception const &ex) {
        if (strcmp(ex.what(), "") != 0) {
            std::cout << "\n" << ex.what() << std::endl;
            std::cout << "****ABORT****\n" << std::endl;
        }
    }
    catch (...) {
        std::cout << "An unknown fatal error has occured. Aborting." << std::endl;
        std::cout << "****ABORT****" << std::endl;
    }

    std::cout << "Closing XsControl..." << std::endl;
    control->close();

    std::cout << "Deleting callbacks... \n" << std::endl;
    for (auto i = mtwCallbacks.begin(); i != mtwCallbacks.end(); ++i) {
        delete (*i);
    }

    std::cout << "Successful exit." << std::endl;
    std::cout << "Press any key to continue." << std::endl;
    while (!_kbhit()) {}

    return 0;
}