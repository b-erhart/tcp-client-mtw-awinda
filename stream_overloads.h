//
// Created by Benedikt on 16.04.2021.
//

#ifndef AWINDASERVERCLION_STREAM_OVERLOADS_H
#define AWINDASERVERCLION_STREAM_OVERLOADS_H

#include <iomanip>

/*! \brief Stream insertion operator overload for XsPortInfo */
std::ostream &operator<<(std::ostream &out, XsPortInfo const &p) {
    out << "Port: " << std::setw(2) << std::right << p.portNumber() << " (" << p.portName().toStdString() << ") @ "
        << std::setw(7) << p.baudrate() << " Bd"
        << ", " << "ID: " << p.deviceId().toString().toStdString();
    return out;
}

/*! \brief Stream insertion operator overload for XsDevice */
std::ostream &operator<<(std::ostream &out, XsDevice const &d) {
    out << "ID: " << d.deviceId().toString().toStdString() << " (" << d.productCode().toStdString() << ")";
    return out;
}

#endif // #ifndef AWINDASERVERCLION_STREAM_OVERLOADS_H
