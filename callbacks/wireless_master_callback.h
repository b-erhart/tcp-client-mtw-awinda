//
// Created by Benedikt on 16.04.2021.
//

#ifndef AWINDASERVERCLION_WIRELESS_MASTER_CALLBACK_H
#define AWINDASERVERCLION_WIRELESS_MASTER_CALLBACK_H

#include <set>

#include "../xsmutex.h"
#include <xsensdeviceapi/xscallback.h>


//----------------------------------------------------------------------
// Callback handler for wireless master
//----------------------------------------------------------------------
class WirelessMasterCallback : public XsCallback {
public:
    typedef std::set<XsDevice *> XsDeviceSet;

    XsDeviceSet getWirelessMTWs() const {
        XsMutexLocker lock(m_mutex);
        return m_connectedMTWs;
    }

protected:
    virtual void onConnectivityChanged(XsDevice *dev, XsConnectivityState newState) {
        XsMutexLocker lock(m_mutex);
        switch (newState) {
            case XCS_Disconnected:        /*!< Device has disconnected, only limited informational functionality is available. */
                std::cout << "EVENT: MTW Disconnected -> " << *dev << std::endl;
                m_connectedMTWs.erase(dev);
                break;
            case XCS_Rejected:            /*!< Device has been rejected and is disconnected, only limited informational functionality is available. */
                std::cout << "EVENT: MTW Rejected -> " << *dev << std::endl;
                m_connectedMTWs.erase(dev);
                break;
            case XCS_PluggedIn:            /*!< Device is connected through a cable. */
                std::cout << "EVENT: MTW PluggedIn -> " << *dev << std::endl;
                m_connectedMTWs.erase(dev);
                break;
            case XCS_Wireless:            /*!< Device is connected wirelessly. */
                std::cout << "EVENT: MTW Connected -> " << *dev << std::endl;
                m_connectedMTWs.insert(dev);
                break;
            case XCS_File:                /*!< Device is reading from a file. */
                std::cout << "EVENT: MTW File -> " << *dev << std::endl;
                m_connectedMTWs.erase(dev);
                break;
            case XCS_Unknown:            /*!< Device is in an unknown state. */
                std::cout << "EVENT: MTW Unknown -> " << *dev << std::endl;
                m_connectedMTWs.erase(dev);
                break;
            default:
                std::cout << "EVENT: MTW Error -> " << *dev << std::endl;
                m_connectedMTWs.erase(dev);
                break;
        }
    }

private:
    mutable XsMutex m_mutex;
    XsDeviceSet m_connectedMTWs;
};


#endif // #ifndef AWINDASERVERCLION_WIRELESS_MASTER_CALLBACK_H
